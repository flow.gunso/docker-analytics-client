#!/bin/bash

SCRIPT=$(basename ${BASH_SOURCE[0]})

usage() {
    echo "Build and push the Docker images to Gitlab's registry."
    echo ""
    echo "$SCRIPT [options]"
    echo
    echo "Options are:"
    echo "  -h, --help      Print this helper."

    if [ "$1" ]; then
        echo ""
        echo "Exited with error: $1"
        exit 1
    else
        exit 0
    fi
}
 
while (( "$#" )); do
    case "$1" in
        -h|--help)
            usage
            ;;
        -*|--*)
            usage "Unknown option $1"
            ;;
        *) # Break to reduce footprint.
            break
            ;;
    esac
done

# Reject if more than one argument provided.
if [ $# -gt 0 ]; then
    usage "Too many build target given."
fi

# Validate the tag against the SemVer.
semver_regex="^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$"
semver_match=$(echo $CI_COMMIT_TAG | grep -P $semver_regex)
if [ -z $semver_match ]; then
    echo "Version tag $CI_COMMIT_TAG does not adhere to SemVer!"
    exit 1
fi

# Capture version components and assign the tag list.
tags=("client")
major=$(echo $CI_COMMIT_TAG | perl -pe "s/$semver_regex/\1/")
minor=$(echo $CI_COMMIT_TAG | perl -pe "s/$semver_regex/\2/")
revision=$(echo $CI_COMMIT_TAG | perl -pe "s/$semver_regex/\3/")
tags+=("client$major")
tags+=("client$major.$minor")
tags+=("client$major.$minor.$revision")

docker login --username $CI_REGISTRY_DEPLOY_USERNAME --password $CI_REGISTRY_DEPLOY_PASSWORD $CI_REGISTRY
docker build --file .ci/files/Dockerfile --tag $CI_PROJECT_NAME:build .
for tag in "${tags[@]}"; do
    docker tag $CI_PROJECT_NAME:build $DOCKER_ANALYTICS_REGISTRY_IMAGE:$tag
    docker push $DOCKER_ANALYTICS_REGISTRY_IMAGE:$tag
done

