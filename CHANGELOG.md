# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!--## **Unreleased**
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security-->

## **0.2.0** | 2021/06/19
### Fixed
- Fix API requests (#4 #5)
### Added
- Gitlab CI job and script to build the Docker image in that repository but publish it to the central repository (!3 !4)

## **0.1.x** | 2021/06/13
- Initial release
